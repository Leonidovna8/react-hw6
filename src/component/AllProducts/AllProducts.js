import React, { useState, useEffect } from 'react';
import Product from '../Product/Product.js';
import { useProductView } from '../../context/ProductViewContext.js';

function AllProducts(props) {
    const [products, setProducts] = useState([]);
    const [cartCount, setCartCount] = useState(0);
    const [favorites, setFavorites] = useState([]);
    const [favoritesCount, setFavoritesCount] = useState(0);
    const { isGridView, toggleView } = useProductView();

    useEffect(() => {
        fetch('/products.json')
            .then(response => response.json())
            .then(data => setProducts(data))
            .catch(error => console.error(error));
    }, []);

    const handleAddToCart = product => {
        setCartCount(prevCount => prevCount + 1);
        props.addToCart(product);
    };

    const handleAddToFavorite = product => {
        setFavorites(prevFavorites => [...prevFavorites, product]);
    };

    const handleAddToFav = product => {
        setFavoritesCount(prevCount => prevCount + 1);
    };

    useEffect(() => {
        const calculatedFavoritesCount = favorites.filter(value => value).length;
        setFavoritesCount(calculatedFavoritesCount);
    }, [favorites]);

    useEffect(() => {
        const updatedProducts = products.map(product => ({
            ...product,
            isFavorite: favorites.some(favProduct => favProduct.id === product.id),
        }));
        setProducts(updatedProducts);
    }, [favorites]);

    return (
        <div className="home-page">
            <button className='table-card' onClick={toggleView}>
                {isGridView ? 'Table View' : 'Card View'}
            </button>
            <main className={isGridView ? 'main-card' : "main-table"}>
                {products.map(product => (
                    <Product
                        addToCart={props.addToCart}
                        onAddToFavorite={handleAddToFavorite}
                        key={product.id}
                        product={{
                            ...product,
                            isFavorite: favorites.some(favProduct => favProduct.id === product.id),
                        }}
                        eel={product}
                        onAddToCart={handleAddToCart}
                        addToFavorites={props.addToFavorites}
                    />
                ))
                    }
        </main>
    </div>
);
}

export default AllProducts;
