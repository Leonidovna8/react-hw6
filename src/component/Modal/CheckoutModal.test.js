import { render, fireEvent, screen, waitFor, act } from '@testing-library/react';
import CheckoutModal from './CheckoutModal'; 
test('CheckoutModal Opens on Button Click', async () => {
    render(<CheckoutModal isOpen={true} setIsOpen={() => {}} onGoToOrder={() => {}} onGoToCatalog={() => {}} />);

    const checkoutButton = screen.getByText(/Перейти до замовлення/i);
    fireEvent.click(checkoutButton);

    await waitFor(() => {
        expect(screen.getByText(/Ваше замовлення прийняте/i)).toBeInTheDocument();
    });
});
