import React from "react";
import ReactDOM from "react-dom";
import { BrowserRouter } from "react-router-dom";
import App from "./App";
import "./index.css";
import { Provider } from "react-redux";
import store from "./store";
import { ProductViewProvider } from "./context/ProductViewContext";

ReactDOM.createRoot(document.getElementById("root")).render(
  
  <Provider store={store}>
    <BrowserRouter>
    <ProductViewProvider>
      <App />
      </ProductViewProvider>
    </BrowserRouter>
  </Provider>
);
