import React, { useState, useEffect } from "react";
import { Route, Routes, useNavigate } from "react-router-dom";
import { useDispatch } from "react-redux";
import { actionProductList } from "./store/actions.js";
import store from "./store/index.js";
import AllProducts from "./component/AllProducts/AllProducts.js";
import Header from "./component/Header/Header.js";
import CartPage from "./Pages/CartPage/CartPage.js";
import FavoritesPage from "./Pages/FavoritesPage/FavoritesPage.js";
import NotPage from "./Pages/NotPage/NotPage.js";
import {ProductViewProvider} from './context/ProductViewContext.js';
import "./App.css";

function App() {
  const navigate = useNavigate();
  const dispatch = useDispatch();

  const [cart, setCart] = useState(
    JSON.parse(localStorage.getItem("cart")) || []
  );
  const [favorite, setFavorite] = useState(
    JSON.parse(localStorage.getItem("favorites")) || []
  );

  const [cartCount, setCartCount] = useState(cart.length);
  const [favoriteCount, setFavoriteCount] = useState(favorite.length);

  useEffect(() => {

    const fetchData = async () => {
      try {
        const response = await fetch("/products.json");
        const data = await response.json();
        dispatch(actionProductList(data));
      } catch (error) {
        console.error("Помилка завантаження даних:", error);
      }
    };

    fetchData();
  }, [dispatch]);


  const addToCart = (product) => {
    const updatedCart = [...cart, product];
    localStorage.setItem("cart", JSON.stringify(updatedCart));
    setCart(updatedCart);

  };

  const addToFavorite = (product) => {
    const updatedFavorites = [...favorite];
    const existingIndex = updatedFavorites.findIndex((item) => item.id === product.id);

    if (existingIndex !== -1) {
      updatedFavorites.splice(existingIndex, 1);
    } else {
      updatedFavorites.push(product);
    }

    localStorage.setItem("favorites", JSON.stringify(updatedFavorites));
    setFavorite(updatedFavorites);
  };

  const removeFromCart = (productId) => {
    const updatedCart = cart.filter((product) => product.id !== productId);
    localStorage.setItem("cart", JSON.stringify(updatedCart));
    setCart(updatedCart);
    setCartCount(updatedCart.length); 
  };
  

  return (
    <ProductViewProvider>
    <>
      <Header cartCount={cartCount} favoritesCount={favorite.length} />
      <Routes>
        <Route index element={<AllProducts addToCart={addToCart} addToFavorites={addToFavorite}/>}/>
        <Route path="/home" element={<AllProducts addToCart={addToCart} addToFavorites={addToFavorite} />}/>
        <Route path="/home/cart" element={<CartPage cart={cart} removeFromCart={removeFromCart}/>} />
        <Route path="/home/favorites" element={<FavoritesPage favorite={favorite} />} />
        <Route path="*" element={<NotPage />} />
      </Routes>
    </>
    </ProductViewProvider>
  );
} 

export default App;
